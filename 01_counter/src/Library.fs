﻿module App

open Elmish
open Elmish.React

open Fable.React
open Fable.React.Props

// MODEL
type Model = int

let init () = 0

// UPDATE
type Msg =
  | Increment
  | Decrement

let update msg model =
  match msg with
    | Increment -> model + 1
    | Decrement -> model - 1

// VIEW
let view model send =
  div
    [ ]
    [ button  [ OnClick (fun _ -> send Increment) ] [ str "+" ]
    ; div [ ] [ str (string model) ]
    ; button  [ OnClick (fun _ -> send Decrement) ] [ str "—" ]
    ]

// WEB APP
Program.mkSimple init update view
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
