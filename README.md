# Web app workshop

## Beskrivelse

Denne workshop er niveauet efter Scratch, og vi skal lave programmer/apps der
kan køre i en web-browser, og nemt kan deles på hele internettet med dine venner
og familie.

Det er tekst-baseret programmering, og vi bruger “Intellisense” der hjælper dig
med at skrive, og undgå mange fejl.

Det kan være simple spil og lidt mere komplicerede programmer, der evt henter
data fra andre hjemmesider.

Alder: 10 til 17 år

Tekniske krav: Windows, MacOS eller Linux (ikke ChromeBook !). Mus, trådløst
net, oplader

## Forudsætninger

For at kunne deltage ved denne workshop, skal følgende software blive
installeret på maskinerne:

* .NET 6.0 (LTS): 
  * https://dotnet.microsoft.com/en-us/download/dotnet/6.0
  
* NodeJS 16 (LTS): 
  * https://nodejs.org/en/download/
  
* Visual Studio Code: 
  * https://code.visualstudio.com/download
  
* Ionide (Intellisense): 
  * https://marketplace.visualstudio.com/items?itemName=Ionide.Ionide-fsharp

> **Bemærkning**: Det er vigtigt at slå fra `inlayHints` fra i `VS Code`. Det
> gøres ved at klikke ved på `File` > `Preferences` > `Settings` og tilføje
> følgende til `JSON` filen `settings.json`. 

![](./img/vscode-disable-hints-preferences-settings.png)

> For at få filen frem, skal du klikke ved denne knap:

![](./img/vscode-disable-hints-preferences-settings-button.png)

```json
{
  …,
  "editor.inlayHints.enabled": "off"
}
```

## 00) Hello, World!

_«Hello, world! er et simpelt computerprogram, der demonstrerer nogle af de
basale elementer i et programmeringssprog. Det eneste, programmet gør, er at
udskrive på skærmen teksten "Hello, world!", og programmet er ofte det første,
en programmør skriver, når han/hun er ved at lære et nyt sprog.»_

> **Kilde**: https://da.wikipedia.org/wiki/Hello_world-program

Det første vi vil gøre ved denne workshop er at udskrive på skærmen `Hello,
world!` som beskrevet overfor. For at kunne gøre dette er følgende skridt
nødvendige:

1. Start en `kommando prompt` eller `terminal` og opret en mappe ved din
   maskine. Helst uden mellemrum:
   ```bash
   mkdir 00_hello-world
   ```
   ```bash
   cd 00_hello-world
   ```
2. Opret en fil som angiver `Software Development Kit` (SDK) som vi vil bruge:
   ```bash
   dotnet new globaljson --sdk-version 6.0.100
   ```
3. Sørg for at vi kan bruge nyere versioner ved at tilføje `rollForward` med `VS
   Code`:
   ```json
   {
     "sdk": {
       "version": "6.0.100",
       "rollForward": "latestFeature"
     }
   }
   ```
4. Opret en lokal `nuget config` fil:
   ```bash
   dotnet new nugetconfig
   ```
5. Opret en lokal `manifest` fil:
   ```bash
   dotnet new tool-manifest
   ```
6. Tilføj `Fable` værktøjet for dette projekt:
   ```bash
   dotnet tool install fable --local
   ```
7. Skab projektet:
   ```bash
   dotnet new classlib --language "F#" --framework "net6.0" --name WebApp --output src
   ```
8. Tilføj de nødvendige `Fable` pakker til projektet:
   ```bash
   dotnet add src/WebApp.fsproj package Fable.Core
   ```
   ```bash
   dotnet add src/WebApp.fsproj package Fable.Elmish.React
   ```
9. Tilføj følgende kode i filen `src/Library.fs` (erstat koden som findes i
   forvejen):
   ```fsharp
   module App
   
   open Elmish
   open Elmish.React
   
   open Fable.React
   open Fable.React.Props
   
   // MODEL
   type Model = string
   
   let init () = 
     "Hello, World!"
   
   // UPDATE
   let update _ model =
     model
   
   // VIEW
   let view model _ =
     div
       [ ]
       [ div [] [ str model ]
       ]
   
   // WEB APP
   Program.mkSimple init update view
   |> Program.withReactSynchronous "elmish-app"
   |> Program.withConsoleTrace
   |> Program.run
   ```
10. Opret en mappen `public` samt under filen `index.html` med `VS Code` med
    følgende indhold:
    ```html
    <!doctype html>
    <html>
    <head>
      <title>Web App</title>
      <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
      <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <div id="elmish-app" class="elmish-app"></div>
        <script src="bundle.js"></script>
    </body>
    </html>
    ```
11. Opret en `package.json` fil:
    ```json
    {
      "private": true,
      "scripts": {
        "postinstall": "dotnet tool restore",
        "start": "dotnet fable clean --yes && dotnet fable watch src --run webpack serve",
        "build": "webpack build"
      },
      "dependencies": {
        "react": "^17.0.1",
        "react-dom": "^17.0.1",
        "webpack": "^5.14.0",
        "webpack-cli": "^4.3.1",
        "webpack-dev-server": "^3.11.2"
      }
    }
    ```
12. Opret en `webpack.config.js` file:
    ```javascript
    var path = require("path");
    
    module.exports = {
        mode: "development",
        entry: "./src/Library.fs.js",
        output: {
            path: path.join(__dirname, "./public"),
            filename: "bundle.js",
        },
        devServer: {
            contentBase: "./public",
            port: 8080,
        }
    }
    ```
13. Byg løsningen og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
14. Tilgå web applikationen ved `http://localhost:8080/` og du burde se følgende
    tekst:
    ```text
    Hello, World!
    ```
15. For at stoppe web applikatinen skal du blot bruge genvejen `CTRL-C` to
    gange.

## 01) Forøge og formindske en tæller

Denne gang, vil vi udvide vores web applikation, fra sidste gang, med noget
logik som gør at vi i stedet for at skrive en simpel tekst på skærmen, vil vi
fremover vise et tal, som vi kan forøge og formindske ved at klikke på nogle
knapper.

![](./img/mt833525_1019_abraham_figure2_the-model-view-update-pattern.png)

**Figur 1: The Model-View-Update Pattern (MSDN Magazine Issues 2019 October)**

1. Start med at tage en kopi af projektet fra sidste gang `00_hello-world`:
   * For `linux` eller `macOS` skriv i en `terminal`:
     ```bash
     cp -Rv 00_hello-world 01_counter
     ```
   * For `windows` skriv i en `kommando prompt`:
     ```bash
     xcopy /e /k /h /i 00_hello-world 01_counter
     ```
2. Åbn filen `01_counter/src/Library.fs` i `VS Code` og erstat den tidligere
   model med:
   ```fsharp
   // MODEL
   type Model = int
   ```
3. I samme fil, `01_counter/src/Library.fs`, erstat den tidligere
   initialiserings model værdi med i tal i stedet for en tekst:
   ```fsharp
   let init () = 0
   ```
4. Tilføj, stadig i samme fil, beskeder som vi vil anvende til at beslutte om vi
   skal øge eller sænke vores model værdi:
   ```fsharp
   // UPDATE
   type Msg =
     | Increment
     | Decrement
   ```
5. Opdater, stadig i samme fil, opdateringsfunktionen med de tilføjede
   beskeder. Modtager vi `Increment`, så vil vi øge vores model værdi med `1` og
   modtager vi `Decrement`, så vil vi sænke tallet med `-1`.
   ```fsharp
   let update msg model =
     match msg with
       | Increment -> model + 1
       | Decrement -> model - 1
   ```
6. Endelig, stadig i samme fil, skal vi tilføje nogle knapper til vores
   visning I første omgang skal vi navngive en `send` besked parameter i vores
   visning funktion. Erstat den tidligere unavngivet parameter med
   ```fsharp
   // VIEW
   let view model send =
   ```
   Nu kan vi tilføje to knapper således:
   ```fsharp
   div
     [ ]
     [ button [ OnClick (fun _ -> send Increment) ] [ str "+" ]
     ; div [] [ str (string model) ]
     ; button [ OnClick (fun _ -> send Decrement) ] [ str "-" ]
     ]
   ```
7. Byg og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
8. Tilgå den ved `http://localhost:8080/` og du burde kunne klikke på `+` og `-`
   knapperne og se hvordan tallet ændre værdi:
    ```text
    ⊞
    42
    ⊟
    ```
9. Husk, for at stoppe web applikationen, skal du blot bruge genvejen `CTRL-C`
   to gange.


## 02) Simpel lommeregner

Denne gang, vil vi omdanne vores web applikation, fra sidste gang, til en
lommeregner. Den vil være meget simpel i det at den kun kan udfører følgende
basale matematiske operationer: addition, subtraktion, multiplikation og
division.

![](./img/winxp-calculator.jpg)

**Figur 2: Lommeregner i MS Windows XP**

1. Som sidste gang, vil vi starte med at tage en kopi af projektet fra sidste
   uge `01_counter`:
   * For `linux` eller `macOS` skriv i en `terminal`:
     ```bash
     cp -Rv 01_counter 02_calculator
     ```
   * For `windows` skriv i en `kommando prompt`:
     ```bash
     xcopy /e /k /h /i 01_counter 02_calculator
     ```
2. Åbn filen `02_calculator/src/Library.fs` i `VS Code` og erstat den tidligere
   model med en lidt mere kompleks, hvor den fremover vil bestå af tre
   værdier. I `state` vil vi gemme det tal som vi vil udfører en operation
   på. Værdien `input` vil vi bruge til at vise det indtastede tal samt
   resultatet ved at trykke på `=`. Og endelig, i `logic` vil vi gemme
   operationen som vi udfører:
   ```fsharp
   // MODEL
   type Model =
     { state : float
       input : string
       logic : float -> float -> float
     }
   ```
   > **Bemærkning**: I stedet for at bruge heltal (`int`) så vil vi bruge
   > decimaltal (`float`).
3. I samme fil, `02_calculator/src/Library.fs`, erstatter vi den tidligere
   initialiserings model værdi med følgende:
   ```fsharp
   let init () =
     { state = 0
       input = "0"
       logic = fun _ _ -> 0
     }
   ```
4. Erstat, stadig i samme fil, beskederne som defineret tidligere, med følgende:
   ```fsharp
   // UPDATE
   type Msg =
     | Clear
     | Comma
     | Digit of float
     | Logic of (float -> float -> float)
     | Equal
   ```
5. Ændre, stadig i samme fil, opdateringsfunktionen med de tilføjede
   beskeder. Modtager vi `Clear`, så nulstiller vi alle værdierne. Modtager vi
   `Comma` så tilføjer vi det kun hvis det ikke allerede er tilføjet til decimal
   tallet. Modtager vi `Digit d`, hvis nuværende `input` er `0`, så erstatter vi
   værdien, ellers tilføjer vi blot tallet. Modtager vi `Logic f`, så opdatere
   vi `logic` med den valgte operation, samt at vi omdanner `input` til et tal
   og gemmer det i `state`. Yderliggere, nulstiller vi `input`. Endelig, når vi
   modtager `Equal`, omdanner vi `input` til et tal og udregner resultatet ved
   at udfører operationen, gemt i `logic`, på `state` og det omdannet `input`
   værdi:
   ```fsharp
   let update msg model =
     match msg with
       | Clear   ->
         { model with
             state =  0
             input = "0"
             logic = fun _ _ -> 0
         }
       | Comma   ->
         { model with
             input =
               if model.input.Contains(".") then
                 model.input
               else
                 model.input + "."
         }
       | Digit d ->
         { model with
             input =
               if "0" = model.input then
                 string d
               else
                 model.input + (string d)
         }
       | Logic f ->
         { model with
             state = float model.input
             input = "0"
             logic =  f
         }
       | Equal   ->
         let s =       model.state
         let i = float model.input
         let r =       model.logic s i
         { model with
             state = 0
             input = string r
         }
   ```
6. Endelig, stadig i samme fil, skal vi tilføje knapper for vores tal: `0`-`9`;
   for komma: `.`; for vores operationer: division, multiplikation, addition og
   subtraktion; for ryd (`C`) af `input`, `state` og `logic`; og for resultat
   udregning (`=`):
   ```fsharp
   // VIEW
   let view model send =
     div
       [ ]
       [ str (model.input)
       ; div
           []
           [ button
               [ OnClick (fun _ -> send (Logic (/)))
               ]
               [ str "÷"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 7))
               ]
               [ str "7"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 8))
               ]
               [ str "8"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 9))
               ]
               [ str "9"
               ]
           ]
       ; div
           []
           [ button
               [ OnClick (fun _ -> send (Logic (*)))
               ]
               [ str "×"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 4))
               ]
               [ str "4"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 5))
               ]
               [ str "5"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 6))
               ]
               [ str "6"
               ]
           ]
       ; div
           []
           [ button
               [ OnClick (fun _ -> send (Logic (+)))
               ]
               [ str "+"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 1))
               ]
               [ str "1"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 2))
               ]
               [ str "2"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 3))
               ]
               [ str "3"
               ]
           ]
       ; div
           []
           [ button
               [ OnClick (fun _ -> send (Logic (-)))
               ]
               [ str "−"
               ]
           ; button
               [ OnClick (fun _ -> send (Digit 0))
               ]
               [ str "0"
               ]
           ; button
               [ OnClick (fun _ -> send Comma)
               ]
               [ str "."
               ]
           ; button
               [ OnClick (fun _ -> send Clear)
               ]
               [ str "C"
               ]
           ]
       ; div
           []
           [ button
               [ OnClick (fun _ -> send Equal)
               ]
               [ str "="
               ]
           ]
       ]
   ```
   > **Bemærkning**: For et tal `d`, angiver vi `Digit d` som parameteren i
   > `send` funktionen. For operationerne, eksempelvis addition, angiver vi blot
   > `Logic (+)`, altså sprogets indbygget operator i parenteser. Og endelig for
   > komma, ryd og resultat, angiver vi henholdsvis `Clear`, `Comma` og `Equal`
   > i `send` funktionen.
7. Byg og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
8. Tilgå web applikationen ved `http://localhost:8080/` og du burde kunne se en
   lommeregner. Afprøv ved at klikke på følgende knapper: `4`, `2`,`.`,`5`, `*`,
   `2` og `=` og du vil nu kunne se tallet `85`.
9. Husk, for at stoppe web applikationen, skal du blot bruge genvejen `CTRL-C`
   to gange.

## 03) Simpel stopur

Denne gang, vil vi omdanne vores web applikation, fra sidste gang, til et
stopur.

![](./img/800px-stopwatch_1810201155_ako.jpg)

**Figur 3: Håndholdt mekanisk stopur (Ansgar Koreng / CC BY-SA 4.0)**

1. Som sidste gang, vil vi starte med at tage en kopi af projektet fra sidste
   uge `02_calculator`:
   * For `linux` eller `macOS` skriv i en `terminal`:
     ```bash
     cp -Rv 02_calculator 03_stopwatch
     ```
   * For `windows` skriv i en `kommando prompt`:
     ```bash
     xcopy /e /k /h /i 02_calculator 03_stopwatch
     ```
2. Åbn filen `03_stopwatch/src/Library.fs` i `VS Code` og erstat den tidligere
   model med en model der er sammensat af to typer. I `state` vil vi gemme typen
   for `Watch`, som i sig selv gemmer værdierne for: `timer`, `minutter`,
   `sekunder` og `centisekunder`. Og i `start` vil vi angive om stopuret skal
   måle tidsrummet:
   ```fsharp
   // MODEL
   type Watch =
     { hours        : int
       minutes      : int
       seconds      : int
       centiseconds : int
     }
   type Model =
     { start : bool
       state : Watch
     }
   ```
   > **Bemærkning**: Type `Watch` skal være defineret før `Model`, ellers kan
   > man ikke bruge den.
3. I samme fil, `03_stopwatch/src/Library.fs`, erstatter vi den tidligere
   initialiserings model værdi med følgende:
   ```fsharp
   let init () =
     ( { start = false
         state =
           { hours        = 00
             minutes      = 00
             seconds      = 00
             centiseconds = 00
           }
       }
     , Cmd.none
     )
   ```
   > **Bemærkning**: Som det nu kan ses, i forhold til sidst, så angiver vi
   > `Cmd.none` som en del af initialiserings model værdien. Dette er vi nød til
   > at gøre fordi fremover, vil vores `web app` udfører en hændelse. Men
   > initielt vil den ikke, derfor vi vælger `none`. Yderliggere er typen
   > `Watch` indlejret i `Model` typen.
4. Erstat, stadig i samme fil, beskederne som defineret tidligere, med følgende:
   ```fsharp
   // UPDATE
   type Msg =
     | Start
     | Stop
     | Clear
     | Centisecond
     | Second
     | Minute
     | Hour
   ```
5. Ændre, stadig i samme fil, opdateringsfunktionen med de tilføjede
   beskeder. Modtager vi `Start`, så starter vi stopuret. Modtager vi `Stop`, så
   stopper vi stopuret. Modtager vi `Clear`, så nulstiller vi alle
   værdierne. Modtager vi `Centisecond`, så opdatere vi modellens ur
   centisekunder (`state`) med `1`. Hvis centisekunderne overgår `99`, så
   angiver `0` og så skaber vi en `Second` besked. Modtager vi `Second`, så
   opdatere vi modellens ur sekunder (`state`) med `1`. Hvis sekunderne overgår
   `59`, så angiver `0` og så skaber vi en `Minute` besked. Modtager vi
   `Minute`, så opdatere vi modellens ur minutter (`state`) med `1`. Hvis
   minutterne overgår `59`, så angiver `0` og så skaber vi en `Hour`
   besked. Endelig, modtager vi `Hour`, så opdatere vi modellens ur timer
   (`state`) med `1`.
   ```fsharp
   let update msg model =
     match msg with
       | Start ->
         ( { model with start = true  }
         , Cmd.none
         )
       | Stop ->
         ( { model with start = false }
         , Cmd.none
         )
       | Clear ->
         ( { start = false
             state =
               { hours        = 00
                 minutes      = 00
                 seconds      = 00
                 centiseconds = 00
               }
           }
         , Cmd.none
         )
       | Centisecond ->
         if model.start then
           let ds = model.state.centiseconds + 1
           ( { model
                 with
                   state =
                     { model.state
                         with
                           centiseconds = ds % 100
                     }
             }
           , if 99 < ds then
               Cmd.ofMsg Second
             else
               Cmd.none
           )
         else
           ( model
           , Cmd.none
           )
       | Second ->
         let ss = model.state.seconds + 1
         ( { model
               with
                 state =
                   { model.state
                       with
                         seconds = ss % 60
                   }
           }
         , if 59 < ss then
             Cmd.ofMsg Minute
           else
             Cmd.none
         )
       | Minute ->
         let ms = model.state.minutes + 1
         ( { model
               with
                 state =
                   { model.state
                       with
                         minutes = ms % 60
                   }
           }
         , if 59 < ms then
             Cmd.ofMsg Hour
           else
             Cmd.none
         )
       | Hour ->
         ( { model
               with
                 state =
                   { model.state
                       with
                         hours = model.state.hours + 1
                   }
           }
         , Cmd.none
         )
   ```
6. For at opdatere visningen, stadig i samme fil, skal vi tilføje stopurets
   viser samt knapper for start, stop og ryd, hvor vi angiver henholdsvis
   `Start` `Stop` og `Clear` i `send` funktionen:
   ```fsharp
   // VIEW
   let view model send =
     div
       [ ]
       [ div
           []
           [ str
               ( sprintf "%02i:%02i:%02i:%02i"
                   model.state.hours
                   model.state.minutes
                   model.state.seconds
                   model.state.centiseconds
               )
           ]
       ; button
           [ OnClick (fun _ -> send Start)
           ]
           [ str "start"
           ]
       ; button
           [ OnClick (fun _ -> send Stop)
           ]
           [ str "stop"
           ]
       ; button
           [ OnClick (fun _ -> send Clear)
           ]
           [ str "clear"
           ]
       ]
   ```
7. Som noget nyt denne gang, skal vi tilføje noget logik som lytter til
   hændelser som ikke er udført af et menneske, men af web applikationen. Tilføj
   følgende lige under `view …` og over for `// WEB APP`:
   ```fsharp
   // SUBSCRIPTION
   let interval milliseconds msg _ =
     Cmd.ofSub
       ( fun send ->
           JS.setInterval
             ( fun _ ->
                 send msg
             )
             milliseconds
             |> ignore
       )
   ```
   > **Bemærkning**: For at vi kan bruge `JS.setInterval` skal vi tilføje `open
   > Fable.Core` oppe i toppen af filen, under `open Elmish.React` og over `open
   > Fable.React`
8. Vi skal nu kalde logikken som vi lige har tilføjet ved at angive i bunden af
   filen følgende `|> Program.withSubscription`. Vi angiver at hver 10
   millisekund, skal vi sende en `Centisecond` besked:
   ```fsharp
   // WEB APP
   Program.mkProgram init update view
   |> Program.withSubscription (interval 10 Centisecond)
   |> Program.withReactSynchronous "elmish-app"
   |> Program.withConsoleTrace
   |> Program.run
   ```
9. Byg og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
10. Tilgå web applikationen ved `http://localhost:8080/`.
11. Husk, for at stoppe web applikationen, skal du blot bruge genvejen `CTRL-C`
    to gange.

## 04) Tilfældige hunde

Denne gang, vil vi omdanne vores web applikation, fra sidste gang, til en
applikation der vil hente tilfældige billeder af hunde.

![](./img/random-doggo.jpg)

**Figur 4: Tilfældigt billede af en hund**

1. Som sidste gang, vil vi starte med at tage en kopi af projektet fra sidste
   uge `03_stopwatch`:
   * For `linux` eller `macOS` skriv i en `terminal`:
     ```bash
     cp -Rv 03_stopwatch 04_dogs
     ```
   * For `windows` skriv i en `kommando prompt`:
     ```bash
     xcopy /e /k /h /i 03_stopwatch 04_dogs
     ```
2. Tilføj yderligere nødvendige pakker til projektet:
   ```bash
   cd 04_dogs
   ```
   ```bash
   dotnet add src/WebApp.fsproj package Fable.Fetch
   ```
   ```bash
   dotnet add src/WebApp.fsproj package Fable.Promise
   ```
   ```bash
   dotnet add src/WebApp.fsproj package Thoth.Json
   ```
3. Åbn filen `04_dogs/src/Library.fs` i `VS Code` og tilføj de nye pakker i
   toppen, lige under `module App`:
   ```fsharp
   module App
   
   open Elmish
   open Elmish.React
   
   open Fable.Core
   open Fable.React
   open Fable.React.Props
   
   open Fetch
   
   open Thoth.Json
   ```
4. I samme fil, `04_dogs/src/Library.fs`, erstatter vi den tidligere model med
   en som blot indeholder to egenskaber. I `state` gemmer vi en mulig værdi af
   et billede link. Og i `error` gemmer vi en mulig fejl værdi, hvis der er sket
   en forhindring i at hente et `JSON` data element:
   ```fsharp
   // MODEL
   type Model =
     { state : string option
       error : string option
     }
   ```
   > **Bemærkning**: Ved typen `_ option` betyder at man enten har `None` som i
   > ingenting, eller man har en værdi af typen `Some …`, som for eksempel `Some
   > "foobar"`.
5. I samme fil, `04_dogs/src/Library.fs`, erstatter vi den tidligere
   initialiserings model værdi med følgende:
   ```fsharp
   let init () =
     ( { state = None
         error = None
       }
     , Cmd.none
     )
   ```
6. Erstat, stadig i samme fil, beskederne som defineret tidligere, med følgende:
   ```fsharp
   // UPDATE
   type Msg =
     | Random
     | Success of string
     | Failure of exn
   ```
7. Fordi vores web app skal kunne midlertidig opbevare `JSON` data elementet som
   vi modtager, er vi nød til at tilføje følgende type lige over vores
   opdateringsfunktion (`let update msg model =`):
   ```fsharp
   type Doogo =
     { url : string
     }
   ```
8. Ændre, stadig i samme fil, opdateringsfunktionen med de tilføjede
   beskeder. Modtager vi `Random`, så henter vi et `JSON` data element fra
   `https://random.dog/woof.json`, som vi omdanner til vores defineret `Doogo`
   type. Hvis de lykkedes, så kalder vi `Success` med linket til billedet,
   ellers, så kalder vi `Failure` med den undtagelse der er blevet kastet. Når
   vi modtager `Success` beskeden, så opdatere vi `state` med linket, hvis det
   ikke er `.MP4` (video) eller `.WEBM` (billede) formatter. Modtager vi
   `Failure`, så opdatere vi `error` med undtagelsens fejl besked.
   ```fsharp
   let update msg model =
     match msg with
       | Random ->
         ( model
         , Cmd.OfPromise.either
             ( fun _ ->
                 fetch "https://random.dog/woof.json" []
                 |> Promise.bind (
                   fun response ->
                     response.text()
                 )
                 |> Promise.map (
                   fun json ->
                     Decode.Auto.fromString<Doogo>
                       ( json
                       , caseStrategy = CamelCase
                       )
                     |> function
                       | Result.Ok    doggo -> doggo.url
                       | Result.Error error -> error
                 )
             )
             ()
             Success
             Failure
         )
       | Success url ->
         if url.EndsWith(".mp4") || url.EndsWith(".webm") then
           ( { model
                 with
                   state = None
                   error = Some "File was MP4 or WEBM. Try again!"
             }
           , Cmd.none
           )
         else
           ( { model
                 with
                   state = Some url
                   error = None
             }
           , Cmd.none
           )
       | Failure ex ->
           ( { model
                 with
                   error = Some ex.Message
             }
         , Cmd.none
         )
   ```
9. For at opdatere visningen, stadig i samme fil, skal vi blot tilføje en enkelt
   knap til at hente de tilfældige billeder, som sender `Random` i `send`
   funktionen. Yderliggere skal vi tilføje et tekst felt som viser de
   fejlbeskeder der kan opstå når vi prøver på at hente billederne. Og vi skal
   også tilføje en billede beholder som vil pege på de link som vi henter ned. I
   begge tilfælde, så viser vi kun indhold, hvis værdierne for `state` og
   `error` ikke er `None`:
   ```fsharp
   // VIEW
   let view model send =
     div
       [ ]
       [ div
           []
           ( match model.error with
               | None     -> []
               | Some err ->
                   [ str (sprintf "Error: %s" err)
                   ]
           )
       ; button
           [ OnClick (fun _ -> send Random)
           ]
           [ str "random doggo(s)"
           ]
       ; div
           []
           ( match model.state with
               | None     -> []
               | Some url ->
                   [ img
                       [ Src url
                         Style
                           [ Width "600px"
                           ]
                       ]
                   ]
           )
       ]
   ```
10. Da vi ikke længere har noget logik som lytter til hændelser som ikke er
    udført af et menneske, behøver vi ikke længere at have
    `Program.withSubscription` ved:
   ```fsharp
   // WEB APP
   Program.mkProgram init update view
   |> Program.withReactSynchronous "elmish-app"
   |> Program.withConsoleTrace
   |> Program.run
   ```
11. Byg og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
12. Tilgå web applikationen ved `http://localhost:8080/`.
13. Husk, for at stoppe web applikationen, skal du blot bruge genvejen `CTRL-C`
    to gange.

## 05) Hent afstemningsområder i KBH

Denne gang, vil vi omdanne vores web applikation, fra sidste gang, til en
applikation der vil hente afstemningsområder i København.

1. Som sidste gang, vil vi starte med at tage en kopi af projektet fra sidste
   uge `04_dogs`:
   * For `linux` eller `macOS` skriv i en `terminal`:
     ```bash
     cp -Rv 04_dogs 05_voting-area-cph
     ```
   * For `windows` skriv i en `kommando prompt`:
     ```bash
     xcopy /e /k /h /i 04_dogs 05_voting-area-cph
     ```
2. I filen, `05_voting-area-cph/src/Library.fs`, erstatter vi den tidligere
   model med en som indeholder disse to egenskaber. I `state` gemmer vi en mulig
   værdi af en liste af afstemningsområder som indeholder både navn og
   adresse. Og i `error` gemmer vi en mulig fejl værdi, hvis der er sket en
   forhindring i at hente et `JSON` data element:
   ```fsharp
   // MODEL
   type VotingPlaceCPH =
     { name    : string
       address : string
     }
     
   type Model =
     { state : VotingPlaceCPH list
       error : string option
     }
   ```
3. I samme fil, `05_voting-area-cph/src/Library.fs`, erstatter vi den tidligere
   initialiserings model værdi med følgende:
   ```fsharp
   let init () =
     ( { state = List.empty
         error = None
       }
     , Cmd.none
     )
   ```
4. Erstat, stadig i samme fil, beskederne som defineret tidligere, med følgende:
   ```fsharp
   // UPDATE
   type Msg =
     | Retrieve
     | Success of VotingPlaceCPH list * string
     | Failure of exn
   ```
5. Fordi vores web app skal kunne midlertidig opbevare `JSON` data elementet som
   vi modtager, er vi nød til at tilføje følgende type lige over vores
   opdateringsfunktion (`let update msg model =`):
   ```fsharp
   type Property =
     { afstemningsstednavn : string
       afstemingsadresse   : string
     }
     
   type Feature =
     { properties : Property
     }
     
   type FeatureCollection =
     { features : Feature list
     }
   ```
6. Ændre, stadig i samme fil, opdateringsfunktionen med de tilføjede
   beskeder. Modtager vi `Retrieve`, så henter vi et `JSON` data element fra
   `https://wfs-kbhkort.kk.dk/k101/ows`, som vi omdanner til vores defineret
   `FeatureCollection` type. Hvis de lykkedes, så kalder vi `Success` med listen
   af afstemningsområderne, ellers, så kalder vi `Failure` med den undtagelse
   der er blevet kastet. Når vi modtager `Success` beskeden, så opdatere vi
   `state` med listen af afstemningsområderne. Modtager vi `Failure`, så
   opdatere vi `error` med undtagelsens fejl besked.
   ```fsharp
   // Home > Københavns Kommune > Voting areas in Copenhagen:
   // 
   // https://www.opendata.dk/city-of-copenhagen/afstemningsomrader-i-kobenhavn
   let url = @"https://wfs-kbhkort.kk.dk"
   let api = @"k101/ows"
   let qry = @"service=WFS&version=1.0.0&request=GetFeature&typeName=k101:afstemningsomraade&outputFormat=json&SRSNAME=EPSG:4326"
   
   let update msg model =
     match msg with
       | Retrieve ->
         ( model
         , Cmd.OfPromise.either
             ( fun _ ->
                 fetch (sprintf "%s/%s?%s" url api qry) []
                 |> Promise.bind (
                   fun response ->
                     response.text()
                 )
                 |> Promise.map (
                   fun json ->
                     Decode.Auto.fromString<FeatureCollection>
                       ( json
                       , caseStrategy = CamelCase
                       )
                     |> function
                       | Result.Ok    featureCollection ->
                         ( featureCollection.features
                           |> List.map (
                             fun feature ->
                               { name    = feature.properties.afstemningsstednavn
                                 address = feature.properties.afstemingsadresse
                               }
                           )
                         , String.Empty
                         )
                       | Result.Error error ->
                         ( List.empty
                         , error
                         )
                 )
             )
             ()
             Success
             Failure
         )
       | Success (features, error) ->
         if String.Empty = error then
           ( { model
                 with
                   state = features
                   error = None
             }
           , Cmd.none
           )
         else
           ( { model
                 with
                   state = []
                   error = Some error
             }
           , Cmd.none
           )
           
       | Failure ex ->
           ( { model
                 with
                   error = Some ex.Message
             }
         , Cmd.none
         )
   ```
7. For at opdatere visningen, stadig i samme fil, skal vi blot tilføje en enkelt
   knap til at hente de tilfældige billeder, som sender `Retrieve` i `send`
   funktionen. Yderliggere skal vi tilføje et tekst felt som viser de
   fejlbeskeder der kan opstå når vi prøver på at hente billederne. Og vi skal
   også tilføje en komponent som kan vise listen af afstemningsområderne,
   sorteret efter navn:
   ```fsharp
   // VIEW
   let view model send =
     div
       [ ]
       [ button
           [ OnClick (fun _ -> send Retrieve)
           ]
           [ str "Hent afstemningsområder i KBH"
           ]
       ; div
           []
           ( match model.error with
               | None     -> []
               | Some err ->
                   [ str (sprintf "Error: %s" err)
                   ]
           )
       ; div
           []
           ( model.state
             |> List.sortBy (
               fun votingPlaceCPH ->
                 votingPlaceCPH.name
             )
             |> List.map (
               fun votingPlaceCPH ->
                 div
                   []
                   [ str
                       ( sprintf "Navn: %s og adresse: %s"
                           votingPlaceCPH.name
                           votingPlaceCPH.address
                       )
                   ]
             )
           )
       ]
   ```
8. Byg og start web applikationen således:
    ```bash
    npm install
    ```
    ```bash
    npm start
    ```
9. Tilgå web applikationen ved `http://localhost:8080/`.
10. Husk, for at stoppe web applikationen, skal du blot bruge genvejen `CTRL-C`
    to gange.
