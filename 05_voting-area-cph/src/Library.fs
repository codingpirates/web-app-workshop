﻿module App

open System

open Elmish
open Elmish.React

open Fable.React
open Fable.React.Props

open Fetch

open Thoth.Json

// MODEL
type VotingPlaceCPH =
  { name    : string
    address : string
  }
  
type Model =
  { state : VotingPlaceCPH list
    error : string option
  }
  
let init () =
  ( { state = List.empty
      error = None
    }
  , Cmd.none
  )

// UPDATE
type Msg =
  | Retrieve
  | Success of VotingPlaceCPH list * string
  | Failure of exn
  
type Property =
  { afstemningsstednavn : string
    afstemingsadresse   : string
  }
  
type Feature =
  { properties : Property
  }
  
type FeatureCollection =
  { features : Feature list
  }

// Home > Københavns Kommune > Voting areas in Copenhagen:
// 
// https://www.opendata.dk/city-of-copenhagen/afstemningsomrader-i-kobenhavn
let url = @"https://wfs-kbhkort.kk.dk"
let api = @"k101/ows"
let qry = @"service=WFS&version=1.0.0&request=GetFeature&typeName=k101:afstemningsomraade&outputFormat=json&SRSNAME=EPSG:4326"

let update msg model =
  match msg with
    | Retrieve ->
      ( model
      , Cmd.OfPromise.either
          ( fun _ ->
              fetch (sprintf "%s/%s?%s" url api qry) []
              |> Promise.bind (
                fun response ->
                  response.text()
              )
              |> Promise.map (
                fun json ->
                  Decode.Auto.fromString<FeatureCollection>
                    ( json
                    , caseStrategy = CamelCase
                    )
                  |> function
                    | Result.Ok    featureCollection ->
                      ( featureCollection.features
                        |> List.map (
                          fun feature ->
                            { name    = feature.properties.afstemningsstednavn
                              address = feature.properties.afstemingsadresse
                            }
                        )
                      , String.Empty
                      )
                    | Result.Error error ->
                      ( List.empty
                      , error
                      )
              )
          )
          ()
          Success
          Failure
      )
    | Success (features, error) ->
      if String.Empty = error then
        ( { model
              with
                state = features
                error = None
          }
        , Cmd.none
        )
      else
        ( { model
              with
                state = []
                error = Some error
          }
        , Cmd.none
        )
        
    | Failure ex ->
        ( { model
              with
                error = Some ex.Message
          }
      , Cmd.none
      )

// VIEW
let view model send =
  div
    [ ]
    [ button
        [ OnClick (fun _ -> send Retrieve)
        ]
        [ str "Hent afstemningsområder i KBH"
        ]
    ; div
        []
        ( match model.error with
            | None     -> []
            | Some err ->
                [ str (sprintf "Error: %s" err)
                ]
        )
    ; div
        []
        ( model.state
          |> List.sortBy (
            fun votingPlaceCPH ->
              votingPlaceCPH.name
          )
          |> List.map (
            fun votingPlaceCPH ->
              div
                []
                [ str
                    ( sprintf "Navn: %s og adresse: %s"
                        votingPlaceCPH.name
                        votingPlaceCPH.address
                    )
                ]
          )
        )
    ]

// WEB APP
Program.mkProgram init update view
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
