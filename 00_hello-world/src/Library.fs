﻿module App

open Elmish
open Elmish.React

open Fable.React
open Fable.React.Props

// MODEL
type Model = string

let init () = 
  "Hello, World!"

// UPDATE
let update _ model =
  model

// VIEW
let view model _ =
  div
    [ ]
    [ div [ ] [ str model ]
    ]

// WEB APP
Program.mkSimple init update view
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
