﻿module App

open Elmish
open Elmish.React

open Fable.Core
open Fable.React
open Fable.React.Props

open Fetch

open Thoth.Json

// MODEL
type Model =
  { state : string option
    error : string option
  }

let init () =
  ( { state = None
      error = None
    }
  , Cmd.none
  )

// UPDATE
type Msg =
  | Random
  | Success of int * string
  | Failure of exn

type Doogo =
  { fileSizeBytes : int
    url           : string
  }

let update msg model =
  match msg with
    | Random ->
      ( model
      , Cmd.OfPromise.either
          ( fun _ ->
              fetch "https://random.dog/woof.json" []
              |> Promise.bind (
                fun response ->
                  response.text()
              )
              |> Promise.map (
                fun json ->
                  Decode.Auto.fromString<Doogo>
                    ( json
                    , caseStrategy = CamelCase
                    )
                  |> function
                    | Result.Ok    doggo ->
                      ( doggo.fileSizeBytes
                      , doggo.url
                      )
                    | Result.Error error ->
                      ( 0
                      , error
                      )
              )
          )
          ()
          Success
          Failure
      )
    | Success (size, url) ->
      if url.EndsWith(".mp4") || url.EndsWith(".webm") || size > 200_000 then
        ( { model
              with
                state = None
                error = Some (sprintf "File was MP4, WEBM or greater than 100KB (%iKB). Try again!" (size/1024))
          }
        , Cmd.none
        )
      else
        ( { model
              with
                state = Some url
                error = None
          }
        , Cmd.none
        )
    | Failure ex ->
        ( { model
              with
                error = Some ex.Message
          }
      , Cmd.none
      )

// VIEW
let view model send =
  div
    [ ]
    [ button
        [ OnClick (fun _ -> send Random)
        ]
        [ str "random doggo(s)"
        ]
    ; div
        []
        ( match model.error with
            | None     -> []
            | Some err ->
                [ str (sprintf "Error: %s" err)
                ]
        )
    ; div
        []
        ( match model.state with
            | None     -> []
            | Some url ->
                [ img
                    [ Src url
                      Style
                        [ Width "600px"
                        ]
                    ]
                ]
        )
    ]

// WEB APP
Program.mkProgram init update view
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
