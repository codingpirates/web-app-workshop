﻿module App

open Elmish
open Elmish.React

open Fable.React
open Fable.React.Props

// MODEL
type Model =
  { state : float
    input : string
    logic : float -> float -> float
  }

let init () =
  { state = 0
    input = "0"
    logic = fun _ _ -> 0
  }

// UPDATE
type Msg =
  | Clear
  | Comma
  | Digit of float
  | Logic of (float -> float -> float)
  | Equal

let update msg model =
  match msg with
    | Clear   ->
      { model with
          state =  0
          input = "0"
          logic = fun _ _ -> 0
      }
    | Comma   ->
      { model with
          input =
            if model.input.Contains(".") then
              model.input
            else
              model.input + "."
      }
    | Digit d ->
      { model with
          input =
            if "0" = model.input then
              string d
            else
              model.input + (string d)
      }
    | Logic f ->
      { model with
          state = float model.input
          input = "0"
          logic =  f
      }
    | Equal   ->
      let s =       model.state
      let i = float model.input
      let r =       model.logic s i
      { model with
          state = 0
          input = string r
      }

// VIEW
let view model send =
  div
    [ ]
    [ str (model.input)
    ; div
        []
        [ button
            [ OnClick (fun _ -> send (Logic (/)))
            ]
            [ str "÷"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 7))
            ]
            [ str "7"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 8))
            ]
            [ str "8"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 9))
            ]
            [ str "9"
            ]
        ]
    ; div
        []
        [ button
            [ OnClick (fun _ -> send (Logic (*)))
            ]
            [ str "×"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 4))
            ]
            [ str "4"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 5))
            ]
            [ str "5"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 6))
            ]
            [ str "6"
            ]
        ]
    ; div
        []
        [ button
            [ OnClick (fun _ -> send (Logic (+)))
            ]
            [ str "+"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 1))
            ]
            [ str "1"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 2))
            ]
            [ str "2"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 3))
            ]
            [ str "3"
            ]
        ]
    ; div
        []
        [ button
            [ OnClick (fun _ -> send (Logic (-)))
            ]
            [ str "−"
            ]
        ; button
            [ OnClick (fun _ -> send (Digit 0))
            ]
            [ str "0"
            ]
        ; button
            [ OnClick (fun _ -> send Comma)
            ]
            [ str "."
            ]
        ; button
            [ OnClick (fun _ -> send Clear)
            ]
            [ str "C"
            ]
        ]
    ; div
        []
        [ button
            [ OnClick (fun _ -> send Equal)
            ]
            [ str "="
            ]
        ]
    ]

// WEB APP
Program.mkSimple init update view
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
