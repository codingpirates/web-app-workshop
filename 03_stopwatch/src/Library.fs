﻿module App

open Elmish
open Elmish.React

open Fable.Core
open Fable.React
open Fable.React.Props

// MODEL
type Watch =
  { hours        : int
    minutes      : int
    seconds      : int
    centiseconds : int
  }
type Model =
  { start : bool
    state : Watch
  }

let init () =
  ( { start = false
      state =
        { hours        = 00
          minutes      = 00
          seconds      = 00
          centiseconds = 00
        }
    }
  , Cmd.none
  )

// UPDATE
type Msg =
  | Start
  | Stop
  | Clear
  | Centisecond
  | Second
  | Minute
  | Hour

let update msg model =
  match msg with
    | Start ->
      ( { model with start = true  }
      , Cmd.none
      )
    | Stop ->
      ( { model with start = false }
      , Cmd.none
      )
    | Clear ->
      ( { start = false
          state =
            { hours        = 00
              minutes      = 00
              seconds      = 00
              centiseconds = 00
            }
        }
      , Cmd.none
      )
    | Centisecond ->
      if model.start then
        let ds = model.state.centiseconds + 1
        ( { model
              with
                state =
                  { model.state
                      with
                        centiseconds = ds % 100
                  }
          }
        , if 99 < ds then
            Cmd.ofMsg Second
          else
            Cmd.none
        )
      else
        ( model
        , Cmd.none
        )
    | Second ->
      let ss = model.state.seconds + 1
      ( { model
            with
              state =
                { model.state
                    with
                      seconds = ss % 60
                }
        }
      , if 59 < ss then
          Cmd.ofMsg Minute
        else
          Cmd.none
      )
    | Minute ->
      let ms = model.state.minutes + 1
      ( { model
            with
              state =
                { model.state
                    with
                      minutes = ms % 60
                }
        }
      , if 59 < ms then
          Cmd.ofMsg Hour
        else
          Cmd.none
      )
    | Hour ->
      ( { model
            with
              state =
                { model.state
                    with
                      hours = model.state.hours + 1
                }
        }
      , Cmd.none
      )

// VIEW
let view model send =
  div
    [ ]
    [ div
        []
        [ str
            ( sprintf "%02i:%02i:%02i:%02i"
                model.state.hours
                model.state.minutes
                model.state.seconds
                model.state.centiseconds
            )
        ]
    ; button
        [ OnClick (fun _ -> send Start)
        ]
        [ str "start"
        ]
    ; button
        [ OnClick (fun _ -> send Stop)
        ]
        [ str "stop"
        ]
    ; button
        [ OnClick (fun _ -> send Clear)
        ]
        [ str "clear"
        ]
    ]

// SUBSCRIPTION
let interval milliseconds msg _ =
  Cmd.ofSub
    ( fun send ->
        JS.setInterval
          ( fun _ ->
              send msg
          )
          milliseconds
          |> ignore
    )

// WEB APP
Program.mkProgram init update view
|> Program.withSubscription (interval 10 Centisecond)
|> Program.withReactSynchronous "elmish-app"
|> Program.withConsoleTrace
|> Program.run
